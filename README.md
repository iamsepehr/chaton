# chaton

chat and messaging application for android 

an android messaging app that has these features:

1.  new user sign up and sign in
2.  adding new friends and accepting friend requests
3.  showing online and offline friends
3.  messaging and chatting with friends
4.  receiving notifications when friend request or new message comes
5.  signing out and exit

i did not add my XML files because you can design this app however you want

and of course you CANNOT use these codes directly in Android Studio or Eclipse. 

you have to make some changes and import some classes. you know the drill ;)